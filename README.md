## Microcontroller Interconnect Network protocol version 2.0


minWiki: http://github.com/min-protocol/min/wiki


This is a simple implementation of the min protocol on atsamb71, which contains a file with all the necessary functions needed to communicate the MCU with the host, also contains a short example with implemented main() and min_application_handler() functions (in the file min/target/main.c) in the "example" branch. This implementation uses USART in ring buffer mode.


To run the example in the "example" branch you should execute the following steps: <br/>
* open MPLAB, create a new project and enable USART1(ring buffer mode) and SYSTICK peripherals.<br/>
* right click on your project -> properties-> XC32 (Global Options) -> Common include dirs-> select the directory "../min/target" and click ok and then apply<br/>
* right click on the Souce Files of your project-> Add Existing Item ...-> select the files "min.c" "min.h" "min_atsamv71.c" in the directory ". ./min/target/ "-> click the select button.<br/>
* copy the code in the min/target/main.c file from the "example" branch to the main.c file of your project<br/>
* in line 13 of the listen.py file enter the port that atsamv71 has connected (in linux it will be something like this '/dev/ttyACM0')<br/>
* build and run the main project in MPLAB, then run the listen.py file at ../min/target/host<br/>


File structure:

    target/	                Embedded code
        min.c               MIN code designed to run on an embedded system (from 8-bit MCUs upwards)
        min.h               Header file that defines the API to min
	min_atsamv71.c      Implementation of the min protocol functions for atsamv71 
	sketch_example1     Arduino sketch for an example program
    host/                   Python code to run on a host PC
        min.py              MIN 2.0 reference implementation with support for MIN via Pyserial
	listen.py           Example program to run on a host and talk to an Arduino board and atsamv71

All software and documentation available under the terms of the MIT License:

	The MIT License (MIT)
	
	Copyright (c) 2014-2017 JK Energy Ltd.
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

